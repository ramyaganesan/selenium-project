Feature: Create lead

Background:
Given Open The Browser
And Max the Browser
And Set the Timeout
And Launch the URL

Scenario Outline: Postive flow for Create lead1
And Enter the Username as <userName>
And Enter the Password as <Password>
And Click on the Login Button
And Click on CRMSFA
And Click Leads
And Click Create Lead 
And Enter Company Name as <companyname>
And Enter First Name as <firstname>
And Enter Last Name as <lastname>
When Click on the Submit Button 
Then Verify the CreatedLead 

Examples:
|userName|Password|companyname|firstname|lastname|
|demosalesmanager|crmsfa|cts|Ram|G|



Scenario: Postive flow for Create lead2
And Enter the Username as DemoSalesManager
And Enter the Password as crmsfa
And Click on the Login Button
And Click on CRMSFA
And Click Leads
And Click Create Lead
And Enter Company Name as cts
And Enter First Name as Swetha
And Enter Last Name as G
When Click on the Submit Button 
Then Verify the CreatedLead