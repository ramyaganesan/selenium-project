package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Createleadsteps {
public ChromeDriver driver;

@Given("Open The Browser")
public void openTheBrowser() {
    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
    driver = new ChromeDriver();
}


@Given("Max the Browser")
public void maxTheBrowser() {
	driver.manage().window().maximize();
}

@Given("Set the Timeout")
public void setTheTimeout() {
 driver.manage().timeouts().implicitlyWait(3000,TimeUnit.SECONDS);
}

@Given("Launch the URL")
public void launchTheURL() {
	driver.get("http://leaftaps.com/opentaps/");
}

@Given("Enter the Username as (.*)")
public void enterTheUsernameAsDemoSalesManager(String data) {
	driver.findElementById("username").sendKeys (data);
}

@Given("Enter the Password as (.*)")
public void enterThePasswordAsCrmsfa(String data) {
	driver.findElementById("password").sendKeys(data);
}

@Given("Click on the Login Button")
public void clickOnTheLoginButton() {
	driver.findElementByClassName("decorativeSubmit").click();
}

@Given("Click on CRMSFA")
public void clickOnCRMSFA() {
	driver.findElementByLinkText("CRM/SFA").click();  
}

@Given("Click Leads")
public void clickLeads() {
  driver.findElementByLinkText("Leads").click();
}

@Given("Click Create Lead")
public void clickCreateLead() {
	driver.findElementByLinkText("Create Lead").click();
}

@Given("Enter Company Name as (.*)")
public void enterCompanyName(String data) {
	driver.findElementById("createLeadForm_companyName").sendKeys(data);
}

@Given("Enter First Name as (.*)")
public void enterFirstName(String data) {
	driver.findElementById("createLeadForm_firstName").sendKeys(data);
}

@Given("Enter Last Name as (.*)")
public void enterLastName(String data) {
	driver.findElementById("createLeadForm_lastName").sendKeys(data);
}

@When("Click on the Submit Button")
public void clickOnTheSubmitButton() {
	driver.findElementByName("submitButton").click();
}

@Then("Verify the CreatedLead")
public void verifyTheCreatedLead() { 
	String text1= driver.findElementById("viewLead_parentPartyId_sp").getText();
	if (text1.contains("Ram"))
		System.out.println("True");
	else System.out.println("False");
}


}
