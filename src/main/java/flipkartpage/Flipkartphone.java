package flipkartpage;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Flipkartphone {
@Test
	public void flipkart() throws InterruptedException {
		
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		//object creation
		ChromeDriver driver = new ChromeDriver();
driver.manage().window().maximize();
driver.get("https://www.flipkart.com");

driver.findElementByXPath("//div[@class='_3Njdz7']/child::button[1]").click();

Actions action = new Actions(driver);
action.moveToElement(driver.findElementByXPath("//span[@class='_1QZ6fC _3Lgyp8']")).perform();
Thread.sleep(3000);
//driver.findElementByXPath("(//span[@class='_1QZ6fC _3Lgyp8'])[1]");
driver.findElementByLinkText("Mi").click();
//driver.findElementByLinkText("Redmi Note 6 Pro (Black, 64 GB)").click();

String title= driver.getTitle();
if (title.contains ("MI")){
	System.out.println ("Title verified succesfully");
}
else System.out.println("Title not verified");
Thread.sleep(3000);
driver.findElementByXPath("//div[text()='Newest First']").click();
Thread.sleep(3000);

List<WebElement> productlist = driver.findElementsByClassName("_3wU53n");
List<WebElement> pricelist = driver.findElementsByXPath("//div[@class='_1uv9Cb']/child::div[1]");
for (int i = 0; i < productlist.size(); i++) {
	System.out.println("The mobile model "+productlist.get(i).getText()+" has a price of "+pricelist.get(i).getText());
}
driver.findElementByXPath("//div[text()='Redmi 6 Pro (Lake Blue, 64 GB)']").click();

Set<String> allwindows = driver.getWindowHandles();
List<String> lst =new ArrayList<>();
lst.addAll(allwindows);
driver.switchTo().window(lst.get(1));

String title2= driver.getTitle();
if (title2.contains ("Redmi")){
	System.out.println ("Title verified succesfully");
	}
Thread.sleep(3000);
String ratings = driver.findElementByXPath("//span[contains (text(),'Ratings')]").getText();
System.out.println(ratings);
String reviews = driver.findElementByXPath("//div [@class='col-12-12']//following::span[1]").getText();
System.out.println(reviews);

driver.close();


	}
}
