package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.MyHome;
import pages.Myleads;
import pages.Viewlead;
import pages.CreateLead;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription = "CreateLead";
		authors = "Ramya";
		category = "smoke";
		dataSheetName = "Input";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead (String username,String password, String companyname,String firstname, String lastname) {		
		new LoginPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin();
		
		new HomePage ()
		.clickcrm();
		
		new MyHome()
		.clickleads ();
		
		new Myleads()
		.createleads();
		
		new CreateLead()
		.entercompanyname(companyname)
		.enterfirstname(firstname)
		.enterlastname (lastname)
		.submitlead();
			
		new Viewlead()
		.verifyfname(firstname);
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
}
