package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead  extends ProjectMethods{
public CreateLead() {
	PageFactory.initElements(driver, this);
}
@FindBy (id="createLeadForm_companyName") WebElement companyname;
@FindBy (id="createLeadForm_firstName") WebElement firstname;
@FindBy (id="createLeadForm_lastName") WebElement lastname;
@FindBy (className="smallSubmit") WebElement submit;

public CreateLead entercompanyname(String data) {
type(companyname, data);
return this;
}
public CreateLead enterfirstname(String data) {
	type (firstname, data);
	return this;
}
public CreateLead enterlastname(String data) {
	type (lastname, data);
	return this;
}
public Viewlead submitlead() {
	click (submit);
	return new Viewlead ();

}
}
