package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods{
	public MyHome () {
	PageFactory.initElements(driver, this);
	}	

	@FindBy (linkText="Leads") WebElement clickleads;
	
	//@Findby (linkText="Create Lead") WebElement CreateLead;
	
	public Myleads clickleads () {
		click (clickleads);
	return new Myleads();
	}
	
}
