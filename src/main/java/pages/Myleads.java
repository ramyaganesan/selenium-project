package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Myleads extends ProjectMethods{
	public Myleads() {
		PageFactory.initElements(driver, this);
	}
	@FindBy (linkText="Create Lead") WebElement createLead;

	public CreateLead createleads() {
	click (createLead);
	return new CreateLead();
	}
}